import yaml
from bamboo.tyaml import Document

class TYamlTest(object):
    def test_walk_and_replace_values_can_translate_variables(self):
        EXAMPLE_DATA = {
            'key1': '$test1',
            'key2': ['{test2}+{test1}'],
            'key3': [{
                '$test1', '$test2'
            }]
        }
        EXAMPLE_VARS = {
            'test1': 'a',
            'test2': 2
        }
        result = Document.walk_and_replace_values(EXAMPLE_DATA, EXAMPLE_VARS)
        assert result['key1'] == EXAMPLE_VARS['test1']
        assert result['key2'] == ["{test2}+{test1}".format_map(EXAMPLE_VARS)]
        assert result['key3'][0] == {'$test1': EXAMPLE_VARS['test2']} # Keys will not be translated
