from pathlib import Path
from bamboo import __version__
import subprocess


def test_version():
    "test version in module is equals to the project file."
    process = subprocess.run(("poetry", "version"), stdout=subprocess.PIPE, cwd=Path(__file__).parent)
    conf_version = process.stdout.decode().strip().split(' ')[1]
    assert __version__ == conf_version
