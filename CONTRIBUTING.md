# Contributing Guide

1. Fork.
2. (Recommended) In your local copy, install git hooks under "docs/dev_helpers/git_hooks/" (copy or link them to ".git/hooks").
2. Make changes to your copy.
3. Fire a merge request to `develop` branch in this project.

## Code style
Use `black` to format your code, in default settings.

## Git hooks
### Installing
Copy or link file(s) under "docs/dev_helpers/git_hooks" to ".git/hooks".

````sh
cp -Rl docs/dev_helpers/git_hooks/* .git/hooks/
````

### Options
#### `hooks.allownonascii`
Allow non-ASCII character.

````sh
git config hooks.allownonascii true
````

#### `hooks.autoformatcode`
Auto format code before commiting.

````sh
git config hooks.autoformatcode true
````
