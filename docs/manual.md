# PyBamboo User Manual
This manual is written for PyBamboo "0.1.0". But most of content should also fit the use of other implementation of bamboo. This manual assume you have basic knownledge to "containers" and how it works.

## Table of Contents
0. Preface
1. Hello Bamboo
    1. Your first configuration
    2. Quick look on configuration
    3. The first run
2. Mount external folder
3. Working modes
    1. Local mode
    2. Daemon-managed mode
4. Templated YAML
    1. Passing arguments into service description
    2. Optional fields
    3. `disk_size` type
5. Designs
    1. Service
    2. Ship
    3. Deployment
