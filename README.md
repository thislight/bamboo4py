# Bamboo4py
Bamboo: container scheduler for human

It's a bamboo implementation in Python.

## Installing

````sh
pip install bamboo4py
````

## Still in early stage
This software is still in early stage. Keep a eye on it while using this program.

## Contributing
Maintainer: `Rubicon Rowe <l1589002388@gmail.com>`

[Contributing Guide](./CONTRIBUTING.md) covered some questions in contributing.

### Goals
* A complete program which can automatically manage local deployments
* Running one deployment over multiple hosts (one control panel)
* Automatically self-management across multiple hosts (typically on 2-10 hosts)
* ~100% test coverage

## License
GNU General Public License, version 3 or later.

    Bamboo4py - container scheduler for human, in python
    Copyright (C) 2021  The PyBamboo Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

See file "CONTRIBUTORS" for all contributors. People who have at least one commit to this project and claimed their names in the "CONTRIBUTORS" file are contributors.

## What is bamboo?
Bamboo is a container scheduler, but for human. Human need a clean & declarative way to describe their ideas, and bamboo is to help.

## Why Bamboo?
### Not A Magic
Bamboo is not a huge-industrial black box works on complex design. You can easily have big pictures of how it will work for you.

### Made For Human
The one simplified architecture can helps everyone -- software developers, server administrators and end users.

## Designs

### Service, Ship and Deployment
Deployment is a environment for services. One ship contains a few of services. Ship can only be **deployed** to deployment. Services can be shared or isolated. Shared services will automatically be shared between different ships if the version does not changed. Every service have different network and pid namespaces, but these containers in a ship is to be promised that they can access each other through network alias. Service may have multiple containers, these containers will be run under same network, PID and IPC namespaces.

### Modes
For different situation, bamboo have different modes:
- Local mode
- Daemon-managed mode (in plan)
- Self-managed mode (in plan, after daemon-managed mode)

In local mode bamboo will run under daemon-less style. One deployment only have one ship running and all changes will be applied instantly. These features are not able to use under local mode:
- Smooth switching
- External services

Daemon-managed mode can use above features, and the mode uses a daemon process to monitor status and perform changes. In daemon-managed mode, bamboo's network works like kubernetes -- one control panel over many nodes. Bamboo will automatically maintains the containers, networks and resources just like the bamboo running in local mode. Instead manage connection between containers and host environments by hand, bamboo introduce "external services" for these specific usage, the external services do not managed by bamboo, but these services can suggest services running layout, request for detailed status, publish ports on hosts, and more.

Self-managed mode could be think as automated daemon-managed mode. Thanks to rope (a application-layer virtual private network library, still in prototype stage), you don't need a manually-building control panel if you are just runing your application over a few hosts. You just need a simple bootstrapping node at the first start, and then all nodes in network will automatically manage themselves. The external services are still available in this mode.
